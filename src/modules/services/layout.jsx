import React from 'react'

import ServicesTable from './services-table';
import AddService from './add-service';
import c from './service.module.css';

const ServicesLayout = () => {
  return (
    <div className={c.wrapper}>
      <AddService />
      <ServicesTable />
    </div>
  )
}

export default ServicesLayout;
