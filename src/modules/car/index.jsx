import React from "react";

import { getInfoByVIN } from '../../services/vinDecoder'

const carModule = {
  loader: async ({ params }) => {
    //There is a Vin code for the car: 1FMCU0F71GUA84706
    // Let's imagine that we get it in the same way as `id` or etc.
    // This code is only for showing the possibility fetch data.
    return {
      id: params.id,
      vinInfo: await getInfoByVIN('1FMCU0F71GUA84706')
    };
  },
  element: () => import("./layout").then((module) => <module.default />),
};


export default carModule;