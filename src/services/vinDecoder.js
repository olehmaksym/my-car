export const getInfoByVIN = (vin) => fetch(`https://vpic.nhtsa.dot.gov/api/vehicles/decodevinextended/${vin.toUpperCase()}?format=json`)
  .then((res) => {
    if (res.status === 200) {
      return res.json();
    }
    throw res;
  });
