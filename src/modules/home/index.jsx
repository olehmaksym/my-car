import React from "react";

const homeModule = {
  element: () => import("./layout").then((module) => <module.default />),
};

export default homeModule;