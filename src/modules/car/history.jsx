import React, { useState } from 'react'
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import { v4 as uuidv4 } from 'uuid';

import Form from '../../components/forms';
import Table from '../../components/table'
import { useServicesStore, useCarStore } from '../../store';

import c from './history.module.css';

const initialForm = {
  serviceId: '',
  millage: '',
};
const mapItems = (v, t) => <MenuItem value={v} key={v}>{t}</MenuItem>;

export const History = ({ car }) => {
  const [open, setOpen] = useState(false);
  const [form, setForm] = useState(initialForm);
  const services = useServicesStore((state) => state.services);
  const history = useCarStore((state) => state.history);
  const addHistoryRecord = useCarStore((state) => state.addHistoryRecord);

  const setFormValue = (e) => setForm({ ...form, [e.target.name]: e.target.value });
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false);
    setForm(initialForm);
  };

  const save = () => {
    addHistoryRecord(car.id, {
      id: uuidv4(),
      date: new Date(),
      ...form
    });
    handleClose();
  }

  const servicesList = services.map((s) => mapItems(
    s.id,
    [s.title, s.make, s.price].join(' | ')
  ));

  const serviceName = ([id]) => {
    const s = services.find(s => s.id === id);
    return s ? [s.title, s.make, s.price].join(' | ') : 'none';
  }

  const fields = [
    { label: 'Service', name: 'serviceId', select: true, children: servicesList},
    { label: 'Millage (km)', name: 'millage'},
  ];

  const rows = [
    { title: 'Title', name: ['serviceId'], component: serviceName },
    { title: 'Millage (km)', name: ['millage'] },
    { title: 'Date', name: ['date'], component: ([d]) => d.toString() },
  ];

  return (
    <section>
      <header className={c.header}>
        <h2>Service history</h2>
        <Button
          onClick={handleOpen}
          className={c.btn}
          variant="outlined"
          size="small"
        >
          Add record
        </Button>
      </header>
      <main>
      <Form
        {... {
          open,
          form,
          fields,
          setFormValue,
          handleClose,
          save,
          title: 'Add history record'
        }}
      />
      {history[car.id] && <Table rows={rows} items={history[car.id]} />}
      </main>
    </section>
  )
}
