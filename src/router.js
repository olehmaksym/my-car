import { ReactLocation } from "@tanstack/react-location";

export const routes = [
  {
    path: "/",
    import: () =>
      import("./modules/home").then((module) => module.default),
  },
  {
    path: "/services",
    import: () =>
      import("./modules/services").then((module) => module.default),
  },
  {
    path: "car",
    children: [
      {
        path: ":id",
        import: () =>
          import("./modules/car").then((module) => module.default),
      },
    ],
  },
];

export const location = new ReactLocation();
