import React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

export default function SimpleTable({ rows, items }) {

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            {rows.map((r, i) =>
              <TableCell key={r.title} align={i === 0 ? 'left' : 'right'}>{r.title}</TableCell>)}
          </TableRow>
        </TableHead>
        <TableBody>
          {items.map((row) => (
            <TableRow
              key={row.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              {rows.map((r, i) =>
                <TableCell key={r.title + row.id} align={i === 0 ? 'left' : 'right'}>
                  {r.component
                    ? r.component(r.name.map((k) => row[k]))
                    : row[r.name[0]]}
                </TableCell>)}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
