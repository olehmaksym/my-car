import React from 'react'
import { Link } from '@tanstack/react-location';
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';

import c from './header.module.css';

export default function Header() {
  return (
    <header className={c.header}>
      <div className={c.logo}>
        <DirectionsCarIcon/>
        <p>MyCar</p>
      </div>
      <nav className={c.nav}>
        <Link to="/">My cars</Link>
        <Link to="/services">Services</Link>
      </nav>
    </header>
  )
}
