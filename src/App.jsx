import { Router, Outlet } from "@tanstack/react-location";

import Header from "./components/header";

import { routes, location } from "./router";
import './App.css';

function App() {
  return (
    <Router routes={routes} location={location}>
      <Header />
      <main>
        <Outlet />
      </main>
    </Router>
  );
}

export default App;
