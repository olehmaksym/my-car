import React from 'react';
import { Link } from '@tanstack/react-location';

import Table from '../../components/table'
import { useCarStore } from '../../store';

export default function CarTable() {
  const cars = useCarStore((state) => state.cars);

  if (cars.length === 0) {
    return <p>Your car list is empty. Use the button at the top to add your first car.</p>
  }

  const rows = [
    { title: 'Make & Model', name: ['id', 'name'], component: ([id, name]) => <Link to={`/car/${id}`}>{name}</Link> },
    { title: 'Year', name: ['year'] },
    { title: 'VIN code', name: ['vin'] },
    { title: 'Fuel type', name: ['fuel'] },
    { title: 'Millage (km)', name: ['millage'] },
  ];

  return (
    <Table rows={rows} items={cars} />
  );
}
