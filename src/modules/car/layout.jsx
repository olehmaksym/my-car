import React from 'react';
import { useMatch } from '@tanstack/react-location';

import { useCarStore } from '../../store';

import { History } from './history';
import { Result } from './result';
import c from './car.module.css';

const CarLayout = () => {
  const cars = useCarStore((state) => state.cars);
  const { id, vinInfo } = useMatch().data;
  const car = cars.find(c => c.id === id);

  if (!car) {
    return <p>Car not found.</p>
  }

  return (
    <div className={c.wrapper}>
      <h1>{car.name}</h1>
      <table className={c.carTable}>
        <tbody>
          <tr>
            <td>Year</td>
            <td>{car.year}</td>
          </tr>
          <tr>
            <td>VIN code</td>
            <td>{car.vin.toUpperCase()}</td>
          </tr>
          <tr>
            <td>Fuel type</td>
            <td>{car.fuel}</td>
          </tr>
          <tr>
            <td>Millage (km)</td>
            <td>{car.millage}</td>
          </tr>
        </tbody>
      </table>
      <History car={car} />
      <Result data={vinInfo?.Results} />
    </div>
  )
}

export default CarLayout;
