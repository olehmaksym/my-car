import React, { useState } from 'react';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import { v4 as uuidv4 } from 'uuid';

import Form from '../../components/forms';
import { useCarStore } from '../../store';

import c from './add-car.module.css';

const mapItems = (v, t) => <MenuItem value={v} key={v}>{t}</MenuItem>;

const fuelTypes = ['Petrol', 'Diesel', 'Electric', 'Hybrid'].map((y) => mapItems(y, y));

const initialForm = {
  name: '',
  year: '',
  vin: '',
  fuel: '',
  millage: ''
};

const fields = [
  { label: 'Make & Model', name: 'name'},
  { label: 'Year', name: 'year'},
  { label: 'VIN code', name: 'vin'},
  { label: 'Fuel type', name: 'fuel', select: true, children: fuelTypes},
  { label: 'Millage (km)', name: 'millage'},
];

export default function AddCar() {
  const [open, setOpen] = useState(false);
  const [form, setForm] = useState(initialForm);
  const setFormValue = (e) => setForm({ ...form, [e.target.name]: e.target.value });
  const addCar = useCarStore((state) => state.addCar);
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false);
    setForm(initialForm);
  };

  const save = () => {
    addCar({
      id: uuidv4(),
      ...form
    });
    handleClose();
  }

  return (
    <>
      <Button onClick={handleOpen} className={c.btn} variant="outlined">Add car</Button>
      <Form
        {... {
          open,
          form,
          fields,
          setFormValue,
          handleClose,
          save,
          title: 'Add new car'
        }}
      />
    </>
  );
}
