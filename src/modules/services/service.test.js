import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ServicesLayout from './layout';

test('Add new service', async () => {
  render(<ServicesLayout />);

  // Find & click on the `Add service` btn
  const btn = screen.getByRole('button', { name: /add service/i });
  userEvent.click(btn);

  // Check if dialog is open
  expect(
    screen.getByRole('heading', { name: /add service/i })
  ).toBeInTheDocument();

  // Fill up form
  const titleInput = screen.getByLabelText('Title');
  const makeInput = screen.getByLabelText('Make');
  const priceInput = screen.getByLabelText('Price');

  userEvent.type(titleInput, 'Change motor OIL');
  userEvent.type(makeInput, 'Ford');
  userEvent.type(priceInput, '102 $');

  // Add new record
  const submitButton = screen.getByRole('button', { name: /save/i });
  userEvent.click(submitButton);

  expect(
    screen.queryByRole('heading', { name: /add service/i })
  ).not.toBe();

  // Check if record exists in the table
  const titleRecord = await screen.findByText('Change motor OIL');
  const makeRecord = await screen.findByText('Ford');
  const priceRecord = await screen.findByText('102 $');

  expect(titleRecord).toBeInTheDocument();
  expect(makeRecord).toBeInTheDocument();
  expect(priceRecord).toBeInTheDocument();
});
