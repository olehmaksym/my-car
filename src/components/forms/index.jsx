import React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import c from './forms.module.css';

export default function Form({
  open,
  form,
  fields,
  setFormValue,
  handleClose,
  save,
  title
}) {
  return (
    <Dialog open={open} onClose={handleClose} >
      <DialogTitle>{title}</DialogTitle>
      <DialogContent className={c.dialog}>
        {fields.map((f) => (
          <TextField
            {...f}
            key={f.name}
            value={form[f.name]}
            onChange={setFormValue}
            variant='standard'
            size='small'
            className={c.input}
          >
            {f?.children}
          </TextField>
        ))}
      </DialogContent>
      <DialogActions className={c.actions}>
        <Button variant="outlined" onClick={handleClose}>Cancel</Button>
        <Button variant="contained" onClick={save}>Save</Button>
      </DialogActions>
    </Dialog>
  );
}
