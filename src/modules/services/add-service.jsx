import React, { useState } from 'react';
import Button from '@mui/material/Button';
import { v4 as uuidv4 } from 'uuid';

import Form from '../../components/forms';
import { useServicesStore } from '../../store';

import c from './add-service.module.css';

const initialForm = {
  make: '',
  title: '',
  price: '',
};

const fields = [
  { label: 'Title', name: 'title'},
  { label: 'Make', name: 'make'},
  { label: 'Price', name: 'price'},
];

export default function AddCar() {
  const [open, setOpen] = useState(false);
  const [form, setForm] = useState(initialForm);
  const setFormValue = (e) => setForm({ ...form, [e.target.name]: e.target.value });
  const addService = useServicesStore((state) => state.addService);
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false);
    setForm(initialForm);
  };

  const save = () => {
    addService({
      id: uuidv4(),
      ...form
    });
    handleClose();
  }

  return (
    <>
      <Button onClick={handleOpen} className={c.btn} variant="outlined">Add service</Button>
      <Form
        {... {
          open,
          form,
          fields,
          setFormValue,
          handleClose,
          save,
          title: 'Add service'
        }}
      />
    </>
  );
}
