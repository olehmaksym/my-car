import React from 'react';

import Table from '../../components/table'
import { useServicesStore } from '../../store';

export default function ServicesTable() {
  const services = useServicesStore((state) => state.services);

  if (services.length === 0) {
    return <p>Your services list is empty. Use the button at the top to add your service.</p>
  }

  const rows = [
    { title: 'Title', name: ['title'] },
    { title: 'Make', name: ['make'] },
    { title: 'Price', name: ['price'] },
  ];

  return (
    <Table rows={rows} items={services} />
  );
}
