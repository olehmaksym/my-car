import React from 'react';

import c from './car.module.css';

const skip = [143, 156, 191];

export const Result = ({ data }) => {
  const mapResult = (items) => items.map((i) => (
    <tr key={i.VariableId}>
      <td>{i.Variable}</td>
      <td>{i.Value}</td>
    </tr>
  ));

  return (
    <>
      <h2>Info by VIN code</h2>
      <table className={c.carTable}>
        <tbody>
          {mapResult(data.filter((r) => r.Value && !skip.includes(r.VariableId)))}
        </tbody>
      </table>
    </>
  )
};
