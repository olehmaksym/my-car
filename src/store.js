import create from 'zustand'
import { persist } from 'zustand/middleware'

let carStore = (set) => ({
  cars: [{
    id: 'c1efcc38-cce0-45bf-9e30-4b257983f6e5',
    name: 'Ford Escape',
    year: '2017',
    vin: '1FMCU0F71GUA84706',
    fuel: 'Petrol',
    millage: '12000'
  }],
  history: {},
  addCar: (car) =>
    set((state) => ({ cars: [...state.cars, car] })),
  addHistoryRecord: (carId, record) =>
    set((state) => {
      let carHistory = state.history[carId];
        carHistory
          ? carHistory.push(record)
          : carHistory = [record];
        return { history: { ...state.history, [carId]: carHistory } };
    }),
})

let servicesStore = (set) => ({
  services: [{
    id: 'c1efcc38-cce0-45bf-9e30-4b2579e3f6e5',
    make: 'Audi',
    title: 'Change oil',
    price: '100$'
  }],
  addService: (service) =>
    set((state) => ({ services: [...state.services, service] })),
})

carStore = persist(carStore, { name: 'user_cars' })
servicesStore = persist(servicesStore, { name: 'user_services' })

export const useCarStore = create(carStore)
export const useServicesStore = create(servicesStore)