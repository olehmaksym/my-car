import React from 'react'

import CarTable from './car-table';
import AddCar from './add-car';
import c from './home.module.css';

const CarsLayout = () => {
  return (
    <div className={c.wrapper}>
      <AddCar />
      <CarTable />
    </div>
  )
}

export default CarsLayout;
